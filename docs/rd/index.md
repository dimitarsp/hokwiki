Research and Development is essential for progressing the business further. Areas in which we should focus our efforts in order to stay current with our competitors are:

- custom tools, as we are already doing for Revit. A significant effort is required for creating better custom tools for Rhino
- research alternative workflows to our everyday tasks. Particularly worryingly is the industry's dependence on Revit and building all of our tools around a software package that we rent. What if Autodesk do exactly what Adobe did and decide to double their prices. We need to be prepared to have alternative workflows, whic may also provide opportunities for different design.
- Sustainability. Joyce is doing a great job for pushing the sustainability agenda further.

This area of the wiki could serve a log for all the effors that are happening in regards to R&D
