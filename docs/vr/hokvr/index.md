---
title: My Document
summary: A brief description of my document.
authors:
    - Waylan Limberg
    - Tom Christie
date: 2019-01-08
# hero: Metadata enables hero teaser texts
disqus: dimitarp
---


HOK is continuously developing a mobile **HOK VR** app, available on the App Store and the Play Store that works in concurrence to an online service called [VisualVocal](https://visualvocal.com/?target=_blank).

The app works by uploading stereo panoramic images, which can be composed in a virtual tour format, and shared with clients via a code that they input through the mobile HOK VR apps.


### Example stereoscopic panoramic image.

<!-- <img src="links/pano04-small.jpg" width=50% > -->


<img src="links/pano04-small.jpg" class="zoom" data-magnify-src="links/pano04-small.jpg" >

# Guide


<div class="gallery">
  <a href="links/vv01.jpg" class="big"><img src="links/vv01.jpg" width=40% alt="" title="Beautiful Image" /></a>
  <a href="links/vv02.png"><img src="links/vv02.png" width=40% alt="" title=""/></a>
  <a href="links/vv03.png"><img src="links/vv03.png" width=40% alt="" title="Beautiful Image"/></a>
  <a href="links/vv04.png"><img src="links/vv04.png" width=40% alt="" title=""/></a>
</div>


!!! note
    Please refer to [Enscape VR guide](../enscapevr/index.md) to find out how to create stereoscropic 360 degree images"
<div style="margin-top:50px;"></div>

<img src="links/vv02.png" class="zoom" data-magnify-src="links/vv02.png" >

- To request access to using visual vocal, talk to Paul Duggleby.
- Once you have access, login to the service. Note that the login is currently seperate than your HOK login details.
- When you login, you will see a list of projects. If you don't have any, you can create a project ![](links/vv02.png)
- Create a project by giving it a name and a short description (optional) ![](links/vv03.png)
- Once created, you should see it in the projects ![](links/vv04.png)
- Click inside of the project. It is empty, so create a new Vv. A Vv is a **VisualVocal walkthrough**, and where you upload your panoramic images. You can have multiple 360 tours in a project -one for the entrance area, another one for the interior common areas, and a third one for the envelope. Each Vv is independent with its own **unique code.**
- Once you have a Vv tour set up, click on it, and you will get the following window ![](links/vv-add01,gif)
- To add another scene, repeat the process
- In each scene, there is the ability to link another scene in a specific location
- Once you have uploaded all the scenes that you would like to use,

!!! tip " In regards to security there is no way for someone with a specific link to gain access into another Vv within the same project."
