The success of this wiki depends on the effort that we put into it as useful resource for as many people as possible. This includes presenting as much information through consistent format and presentation, which requires a significant amount of effort. For this purpose, we need colleauges that are motivated to share their knowledge to contribute.

# How to contribute

If you are interested in contributing follow these steps:

## Installation process

The platform we are currently using is called [mkdocs](http://www,mkdocs.org). All information is written in [markdown](https://dillinger.io/) format, which allows the flexibility to move to a different platform, as the wiki evolves. Most modern wikis use the same formatting, so the process should be seamless.

### Installing dependencies

Note: the following dependencies all install in your local profile without requiring admin access and without the elevated privilege to seriously compromise your machine. 

1. Install Python 3.7.x or later. Python is a high-level, easy to understand scripting language, similar to javascript. Don't worry, if you are not familiar with coding, you won't need to learn. It's just a dependency for MKDocs.
     - Download python from [this link](https://www.python.org/ftp/python/3.7.2/python-3.7.2-amd64.exe)
     - When installing, install only for local user and check the `add to PATH` option ![win-py-install](links/win-py-install.png)
     - open the command prompt by searching for `cmd` in the start menu and type `python` to make sure it's installed correctly
1. Install mkdocs package
     - in the command prompt, type `pip install mkdocs`. You should now have mkdocs installed in your user folder
     - For more information, consult the mkdocs documentation: [https://www.mkdocs.org/#installation](https://www.mkdocs.org/#installation)
1. Install the material theme.
     - we are currently using the material theme for the wiki. Install it by typing in the command promt `pip install mkdocs-material`
1. Install git. Git is a version control system that allows multiple people to contribute to a project (e.g. the folder in which the wiki is located).
     - Link for downloading the [Git portable version](https://github.com/git-for-windows/git/releases/download/v2.20.1.windows.1/PortableGit-2.20.1-64-bit.7z.exe)
1. Markdown files can be beautifully previewed in a variety of apps, both online, and offline. For the purposes of this guide, we will be using **Visual Studio Code**, as it also works seamlessly with git. 
     - Install [Visual Studio Code](https://code.visualstudio.com/).

### Create a working copy of the wiki with git

 The next step is to clone the wiki folder from the server

- in a command prompt, type `git clone "x:\parametric design\wiki" "c:\users\YOUR.NAME\documents\wiki" ` Replace `YOUR.NAME` with your login name. Now we have duplicated the folder and we are ready to contribute

## Using MKDocs 

The structure of mkdocs consists of a `docs` folder, which is where we will be working by editing markdown files and the `site` folder, which is compiled website based on the files in the docs folder. End users will see the html files located in the site folder.

With everything correctly set up, all we need to do is open the working files and edit. Let's test drive to working process:

- Open visual studio code and click the blue `Open Folder` button to open your local wiki folder: `"c:\users\YOUR.NAME\documents\wiki"`
- Now go to and open `docs/about/contribute/new-user.md`
- To see  preview of the file, click the third button from right to left in the top right-hand corner ![vs-preview](links/vs-preview.PNG) Note that this preview is only of the markdown formatting, not of the actual website. 

### Previewing changes on the actual website

Now, let's see the actual preview of the website

- in the local wiki folder in file explore, type `cmd` and press enter in the folder location bar
![cmd01](links/cmd01.PNG)
- the command prompt should open with the correct location 
- in the command prompt type `mkdocs serve --dev-addr=localhost:5390` and leave the command prompt open.
- Now open Chrome or Edge, and in the url, type `localhost:5390` and you should see  a preview of the wiki. Feel free to navigate a page you are currently editing. 
- Once, we save a file in visual studio code, the wiki will automatically reload in your browser. 
- Once satisfied with the changes you have made, let's build the website into static html files that anyone can access. Open a new instance of terminal in your local wiki folder and type `mkdocs build`

## Committing changes

Now that the are changes in the local working copy, we need to "push" them back to the master copy on the server.  We can either commit from the command prompt or from Visual Studio Code

#### Commit with VS Code
![vs-commit](links/vs-commit.png)

- In VS Code, select the icon indicated above
- Now type a message describing the changes you've made and press `ctrl+enter`
- Press `Yes` on the pop-up that you will get as shown below: ![vs-commit-message](links/vs-commit-message.PNG)
- Now you've sent your changes which someone responsible for the project will review and commit to the master branch

#### Commit with command prompt

- open `cmd` in your local working folder
- enter `git add *` to add all new or modified files to the list
- enter `git commit -a -m "Your Message"`


# Quick Markdown Guide

Note: for the full basice syntax, visit the [Markdown Guide](https://www.markdownguide.org/basic-syntax)

Here we will cover the basics of markdown for you to be able to edit. For more complete information, see the `docs/about/contribut/new-user.md` file both in the browser and in visual studio code.

 ## Headings
 
headings help mark different sections, and are visible in the table of contents

```
# Heading 1
## Heading 2
### Heading 3
#### Heading 4
```

## Links and images

- link: `[link text](http://www.your.url)`
- image: `![image caption](links/img01.jpg)`

Note that all images are relative to the file being edited as shown in the example above. They should reside in the `links` folder

### Bold and Italics

- *Italics*: `*Italic text*`
- **Bold**: `**Bold text**`
- ***Bold and Italic***: `***Bold and Italic***`

### Lists

Ordered list:
```
1. item one
1. item two
1. item three
```
the numbers for the ordered list can either be `1., 2., 3` or `1., 1., 1.,`Both will look the same

Unordered list:
```
- item one
- item two
- item three
```
Nested items:
```
- main element
    - nested element 
```

### Further Markdown information

The following are great resources for editing markdown:
- [Markdown Guide](https://www.markdownguide.org/)


# Adding new wiki content

So far we talked about editing existing files, but what if you would like to add a new wiki subpage? 

- The first step would be to create a new subfolder in the `docs` directory. E.g. `docs/revit/new-section/`
- In the new directory, create a new sub-folder called `links` and a new file called `index.md`
- To add the new page to the navigation, open `mkdocs.yml` located the root of your local wiki folder, and add the full directory link within the nav subfolder: ![mkdocs-links](links/mkdocs-links.PNG)

# Testing new features and services

We have and will continue to explore the best way to organise and present the wiki. With git, we can create different branches, which are independent copies of the folder to explore design, content and usability, without affecting the master branch. More information to come soon.

It is also my intention to record our development process with other services and our experience with these services - what works, what doesn't, what's better than what we're currently using, etc.

Eventually, we will have this wiki properly hosted, which will enable us to explore platforms with a **Graphical User Interface** like [**Grav CMS**](https://www.getgrav.org) that would be easier for less tech-savy users to contribute. 