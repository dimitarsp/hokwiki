#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <https://www.gnu.org/licenses/>.

import Rhino
import scriptcontext as sc
import rhinoscriptsyntax as rs
import Eto

# ------- get data ------------------

layersTop=[i for i in rs.LayerNames() if "::" not in i]

# layers to keep on
layerConst = rs.MultiListBox(layersTop, title="Layers to keep visible", message="Select top level layers to keep visible for all options")

# layers with options
layerOp = rs.MultiListBox(layersTop, title="Layers with options",message="Select top level layers with options to cycle through")

# views
views = rs.MultiListBox(rs.NamedViews(), title="Views",message="Select views to export")

# folder to save names
loc = rs.BrowseForFolder(folder=rs.DocumentPath,title="Folder to save", message="Select folder to save views")


# to do: add options for size and scale
# to do: add viewport style

def captureViewOptions(folder,img):
    e_str= "-ViewCaptureToFile "
    e_str+= "_Unit=pixels "
    e_str+= "_Width=1920 "
    e_str+= "_Height=1080 "
    e_str+= "_Scale=1 "
    e_str+= "_DrawGrid=_No "
    e_str+= "_DrawWorldAxes=_No " 
    e_str+= "_DrawCPlaneAxes=_No "
    e_str+= "_ScaleDrawing=_Yes "
    e_str+= "_TransparentBackground=_No "
    e_str+= folder
    e_str+= "\\"
    e_str+= img
    e_str+= ".png "
    e_str+= "_Enter"
    print e_str
    return e_str

def outputViews(views,loc,option):
    for i in views:
        rs.RestoreNamedView(i)
        if " " in i:
            viewNameFixed = i.replace(" ", "_")
        else:
            viewNameFixed = i
        rs.Command(captureViewOptions(loc,option+"-"+viewNameFixed))

# ------- do something pretty with the data ---------
rs.EnableRedraw(False)

# turn on all layers that stay the same
for i in layerConst:
    rs.LayerVisible(i,visible=True)

# change active layer to a layer we know will stay on
activeLayerOrig = rs.CurrentLayer()
rs.CurrentLayer(layerConst[0])

# turn off all options
for i in layerOp:
    rs.LayerVisible(i,visible=False)

# turn layers on one by one and export views and then turn off
for i in layerOp:
    rs.LayerVisible(i,visible=True)
    prettyOpName=i.replace(" ", "_")
    outputViews(views,loc,prettyOpName)
    rs.LayerVisible(i,visible=False)

# restore active layer
rs.CurrentLayer(activeLayerOrig)

rs.EnableRedraw(True)

