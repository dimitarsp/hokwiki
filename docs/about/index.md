This wiki is an effort to record day-to-day software related processes that can make our job as designers easier. It contains tips, tricks, scripts, best practices, and references to essential information like architectural visualisation companies, VR, etc.

Our vision is for this wiki to grown into an extensive easy-to-use resource that provides quick acess to day-to-day worklflows, basic 3d assets, examples of diagrams, technical drawings, etc.

