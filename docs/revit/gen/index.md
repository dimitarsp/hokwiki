General Revit information.

This section is currently a supplement to the [HUB's main section about Revit](https://hub.hok.com/buildingsmart/educationwiki/Revit.aspx). Here is a snippet of the sections available on the HUB:

![](links/revit-hub.png)
