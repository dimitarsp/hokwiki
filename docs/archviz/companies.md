Here, we'll put Everything related to architectural visualisations, including in-office champions, and our list of trusted visualisers

Here are the companies we have worked with and would like to work with again.
A couple of tips when working with visualisation companies:

- contact them well in advance to see whther they are available. Visualisation studios can be quie busy and very hectic, since their primary clientes are architects :) The more lead time, the better the results, usually for a similar price

List of companies (click for more details + Examples):

- [Pixelflake](#pixelflake), London based
- [Wire Collective](#wire-collective), London based
- [The Boundary](#the-boundary), London based
- [Brick visual](#brick-visual), based in Budapest
- [Meshroom](#meshroom), based in Bulgaria
- [Pictury](#pictury), based in Spain

# Pixelflake

London based

Examples of office projects:

**Kuwait CMA Tower competition**


![image](links/pixelflake/18050_202_Fisherman_Pier.jpg)

<img src="links/pixelflake/18050_102_Oasis_View.jpg" width=45%>
<img src="links/pixelflake/18050_203_VIP_Entrance.jpg" width=45%>

<img src="links/pixelflake/18050_206_Aerial_002.jpg" width=45%>
<img src="links/pixelflake/18050_104_VIP_Hall.jpg" width=45%>


Best contact in the office: David Weatherhead
Company contact:
Website:
Note: they only accept 3dsMax files, so we will need to convert the files in-house.

# Wire Collective

London based, capable of doing both quick atmospheric images, and photorealistic stills

Examples of office Projects

Racetrack

![](links/wirecollective/1627-01a-AERIAL.jpg)

![](links/wirecollective/racetrack/1627-02a-BOX.jpg)

Kuwait CCSE (to do)
Darby Theatre (to do)
Yas Arena (to do)

Examples from non HOK work:

<img src="links/wirecollective/1607-04a-WASHINGTON2.jpg" width="45%">
<img src="links/wirecollective/1703-01e-STATION.jpg" width="45%">

<img src="links/wirecollective/1810-04a-BIRD-wico.jpg" width="45%">
<img src="links/wirecollective/1515-05i-INTERSECTION.jpg" width="45%">


Best contact in the office: Dimitar
Website: http://www.wirecollective.com/
Company contact: Carlos Mazon, director, 0208 144 2713, cm@wirecollective.com

# The Boundary

Very high end, good for projects with high budget

# Brick Visual

- Large Hungarian based company with reasonable prices. Team is large enough to handle stills/animations/vr/ar all at the same time.
- We haven't worked with them yet, but we are impressed with their presentation and large team.
- A nice option with them that they mentioned is that we can hire two visualisers for example for a month work on large/complex projects or two have architectural visualisation be part of the design process

Example projects:

<object data="../../resources/archviz/BrickVisual_SpecialSelection.pdf" type="application/pdf" width="100%" height="400px">
</object>

- <a href="../../resources/archviz/BrickVisual_SpecialSelection.pdf" target="_blank">Open example projects pdf separately</a>
- <a href="../../resources/archviz/BrickVisual_PriceList.pdf" target="_blank">Pricelist</a>
- <a href="../../resources/archviz/BrickVisual_Portfolio2018.pdf" target="_blank">Open example projects pdf separately</a>
- <a href="../../resources/archviz/BrickVisual_BrickVisual_Modularis.pdf" target="_blank">They also do physical models.</a>

Company contact: Anna Bartke, key account manager, +36 30 253 5159, anna.bartke@brickvisual.com

# Meshroom

Bulgarian based, quite experienced

Examples from office projects:

T6
![](links/meshroom/t6.jpg)

Gateshead
![](links/meshroom/gateshead01.jpg)

![](links/meshroom/gateshead02.jpg)


Best contact in the office: Nuno



# Pictury
