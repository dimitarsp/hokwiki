Enscape is a powerful real-time rendering plugin for Revit, Rhino, and Sketchup. In addition to being able to quickly visualise Models interactively, there are two ways to work with VR in Enscape: **tethered** and **mobile**

!!! note "For general Enscape usage refer to the archviz guide"

In comparison to Vray, Enscape is much more user-friendly  and allows anyone to start visualising interactively with teh click of a button



## Mobile VR Panoramas

At this stage, Mobile VR consists of exporting panoramic 360 degree images and uploading them to an online service, Samsung phone, or Oculus Go.

To export 360 images from Enscape:

- Start Enscape
- Find a view that you like
- Select `create 360 degree stereo image` from the enscape toolbar. Enscape will render the stereo image in high res, so it will take a minute or two. ![](links/enscape01.png) Note that the image options may be under a different tab in your program.
- Now go to the panoramas viewer, by clicking the `View and manage your panoramas button` ![](links/enscape02.png)
- To export to a service like the **HOK 360 App or other online viewer**, press the 3 dots icon ![](links/enscape04.png) and save to file  ![](links/enscape02.gif)
- You can also export directly to **Enscape's online viewer** and generade a link/QR code that you can share and place in Print documents. ![](links/enscape03.png) ![](links/enscape05.png)



<div style="margin-top:70px;"></div>

**Overview of the whole process:**

![](links/enscape01.gif)

## Tethered VR

Using Enscape with a tethered VR Headset is as simple as pressing a button.

In order to use VR with Enscape, you need to be using either the **Desktop computer next to IT** with HTC Vive or the **mobile workstation** with Oculus. Ask IT for access to the mobile workstation .

Once you are using one of the VR enabled workstations, enable VR mode in the enscape tab, and start Enscape.

**Revit**

![](links/revit.png)

**Rhino**

![](links/rhino.png)

**Sketchup**

![](links/sketchup.png)

You can also export exe files that contain an interactive model that you can send to clients.

!!! warning
    Due to admin rights and potential danger of opening exe files, the only place from which you can open them is in `B:\Temp\Visualization`

Happy VRing!

!!! tip
    A tethered VR headset is a headset that is connected to a computer with cables
    ![](links/Tethered-VR-Headsets.png)
