The web is becoming an operating system in itself. Almost anything that can be done on your desktop can now be done in a web browser. A great example of this are javascript libraries for immersive and interactive presentations.

# impress.js

### Intro

[Impress JS](https://impress.js.org) is an open source libary that provides a high amount of interactivity, adopting the infinite canvas paradigm. In this sense, it is similar to Prezi.

Here is an _example (add link)_ that was done by David and Dimitar for one of the HOK vision computational design presentations.

Here are some amazing examples of presentations with impress.js:


### Getting started

Currently, Impress JS does not have a graphical user interface, however it is very easy to get started with it. Download the [github](https://github.com/impress/impress.js/) repo, unzip it and open index.html in your favourite text editor ([Atom](https://atom.io) is a great text editor for lightweight scripting and coding).



# reveal.js
