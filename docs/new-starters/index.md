If you are new to HOK, below will be essential information to get you started

Here is a link to the technology orientation found on HOK's hub:
![](links/img01.png)

Here is a pdf link to the [London Handbook](https://hub.hok.com/HR/Documents/Employee%20Handbook%20-%20London.pdf)


## Who to contact for...

- **HR** - Emily Cooper
- **BIM** - Paul Duggleby
- **Accounts** - Andrew Childs
- **Sustainability** - Joyce Chan
- **Office** - Andrea Manning
