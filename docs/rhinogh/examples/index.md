

Below are example projects that we have done with Rhino and  Grashhopper. This list will grow to show various possible usages of how to use the software


# Porstmouth Masterplan

![](links/image23.jpg)

In this masterplan, the footprints were created in rhino and then plugged into grasshopper to generate floors, and areas

![](links/image4.png)

![](links/image3.gif)

# Valencia Arena

## Service yard analysis

In this example, a simple grasshopper file was created to test various service level buildups and analysis of the service yard levels
![](links/image29.png)

![](links/img30.png)

![](links/image17.gif)

## Envelope
The envelope is currently a work in progress, and it is continuosly being refined
![](links/facade01.jpg)


# Cairo Masterplan Proposal

A proposal for competition that HOK has won to develop the masterplan in a new district near the Giza Pyramids
![](links/image25.jpg)

This is a masterplan proposal in which the roads were built manually.


![](links/image19.jpg)

Below is the manual work, consisting of the road network

![](links/image18.jpg)

In order to create an efficiient layout, we created an offset from the buildings and used blocks to replicate them into this:

![](links/image22.jpg)


Here is a part of the script

![](links/image20.jpg)


# Nairobi Facade

![](links/image26.jpg)

In this case, the massing was imported from Revit and subdivided to create the following

![](links/image21.jpg)

Script
![](links/image12.jpg)

# Omani Rivera

![](links/image7.jpg)


In this case, all the buildings need to be drop to the terrain

![](links/image13.png)

# Kuwait tower

[to do]
