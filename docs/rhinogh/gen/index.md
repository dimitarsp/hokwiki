
# New in Rhino 6
Rhino 6 includes some new features that will significantly improve our workflow.

## Viewport rendering, arctic mode

![](links/arctic-color.jpg)


The most important new feature is the ability to do instant viewport rendering with quality almost on par with Vray for simple diagrammatic illustrations.

A new style called **arctic colour** has been created that renders the material's color and edges. To download Arctic colour, use this [link](links/arctic-color.ini)

To install it in your local rhino, go to **Options -> View -> Display Modes** and click **Import** and go to the folder above, and select **Arctic-Color.ini** file.

<img width="50%", style="float:left;padding-top:0; padding-right:10px;" src="links/visual-style-import.jpeg">

### New Render Engine

Cycles, Blender's rendering engine has been packaged in Rhino 6. This means that it is no longer necessary to use Vray for higher quality rendering output. It is called the default Rhino Render

### User Attributes

With version 6, Rhino is one step closer to BIM. Every object can have custom attributes. It's possible to then select specifically by the user key or value assigned.

To access user attributes, select one or multiple objects, and then in the properties, select the Attribute User Text icon:

![](links/user-attributes.png)

There are new selection options that allow to select by either key, value, or both:

![](links/user-attributes-select.png)

### Named Positions
With named positions, it's possible to do an exploded axon that can then placed back together within the same file.

Named positions are similar to named views, and to access them right click near the properties tab title and select named views from the dropdown menu. The short animation below describes the process

![named positions](links/named-positions.gif)



## Custom buttons

To create a new button, right click in an empty space of the main toolbar as shown below

<img src=links/rightclik-add.png width=500px>

You will then get an editor looking like this:

<img src=links/edit-button.png width=500px>

The information below will cover what to place where for each of the buttons


#### Turn Layers off
Turn off the layers for the currently selected objects

Icon: <img src=links/layers-icon.png width=100px>

Text: `Turn off layers`

**Left Mouse Button**

Tooltip: `Turn off layers of selected objects`

Command:
```
-_RunPythonScript (
import rhinoscriptsyntax as rs
objs = rs.GetObjects(message="Select objects whose layers to turn off", preselect=True)

for i in objs:
 lay = rs.ObjectLayer(i)
 rs.LayerVisible(lay,  visible=False)
)
```

**Right Mouse Button**

Tooltip: `Turn off all but selected objects' layers`

Command:
```
-_RunPythonScript (
import rhinoscriptsyntax as rs

for i in rs.LayerNames():
 if i != "_blank":
     layer =  rs.AddLayer(name="_blank")
 else:
     layer = rs.LayerName("_blank")

rs.CurrentLayer(layer)

objs = rs.SelectedObjects()

layon = []
layoff = []

rs.EnableRedraw(False)

for i in objs:
 a = rs.ObjectLayer(i)
 if a not in layon:
     layon.append(a)


for i in rs.LayerNames():
 if i not in [j for j in layon]:
     if i != "_blank":
         rs.LayerVisible(i, visible=False)

rs.EnableRedraw(True)
)
```

#### Make object's layer current
Turn off the layers for the currently selected objects

Icon:
<img src=links/current-layer.png width=100px>

Text: `Make object's layer current`

**Left Mouse Button**

Tooltip: `Make object's layer current`

Command:
```
-_RunPythonScript (
import rhinoscriptsyntax as rs
obj = rs.GetObject(preselect=True)
lay = rs.ObjectLayer(obj)
rs.CurrentLayer(layer=lay)
)
```

## Layouts
Layouts provide important features for printing scaled plans, sections and elevations directly from Rhino.

- creating a new layout
- setting the scale
- controlling the visibility of layers specific to the layout

Layouts go hand in hand with layer settings, such as lineweights and colours

## Layer settings
It is recommended to modifiy layer settings as opposed to individual object settings in order to provide a standarised way of working with linweights and colours within the team.

#### Lineweights

_add screenshot_

#### Colours
_add screenshot_


#### Hatch fills
_add screenshot_
