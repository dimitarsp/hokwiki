In this list, we will place useful Rhino scripts that can significantly speed up workflows. To run the scripts, in the Rhino command prompt, type `editpythonscript`


# Batch views export

The following script automates the process of exporting multiple views for multiple options.
![](links/batchviews06.png)

Example of Roof studies for Valencia

## Prerequisites

In order to use the srcipt usefully, the following prerequisites must be met

- Named views should be saved prior to executing the script
- All options through which to cycle through need to be inside of top level layers as in the image below

![](links/batchviews01.png)

## Usage

- Right-click on the following [script link](links/export-views-options.py) and save to your local machine.
- In the Rhino perspective viewport, choose the rendering style that you would like your saved views to use. Usually, arctic
- In the Rhino command prompt, type `editpythonscript`. A new script editor should open
- Open the saved python file in the Rhino Python script editor and press the green play button
- In the first popup, select the layers you wish to always keep visible. These could be context layers, etc.

![](links/batchviews02.png)

- In the second popup, select the layers with the options you wish to cycle through

![](links/batchviews03.png)

- In the third popup, select the views you wish to cycle through for each option

![](links/batchviews04.png)

- in the fourth and last popup, select the location to save your views. Currently, there is bug in the script that does not allow to save on the server, the best way is to create a temporary folder on your desktop to save the images into. Then copy and paste the images onto the server.

![](links/batchviews05.png)

- the naming out the output file is composed of the option name and the view name, for example **Option01-View01_Birds_Eye.png**
- the resolution is preset in the script at 1920x1080. It can easily be changed by modifiying lines 42 and 43 of the script.

![](links/batchviews08.png)

## Example output

Here are examples of how options can be saved at a fraction of the time as compared to manually going thourgh and saving each view for each option.

**Example of Envelope panelisation studies for Valencia**

![](links/batchviews07.png)

## Further Development
Note that the current UI is a temporary work in progress while implementing the proposed UI as shown below

![](links/batchviews-eto.png)

A few more notes. In this script, saved layer states were omitted due to the ever-changing nature of our files. For example, when a new layer for a new option is added, all the layer states will have to be updated in order to take into account new layers, thus adding significant time to the process.

# Badger Batch VRay output

This is a script that Carlos developed for batch rendering views with VRay. Previously, we used this script extensively for outputting a series of diagrams. However, with the new Arctic mode in Rhino 6, the process has become significantly faster. Nevertheless, this batch render tool is still useful for higher quality renderings.

![](links/badger01.png)


## Usage
- to install, open this folder `x:\Parametric Design\wiki\site\rhino\scripts\links\` and drag `badgerRender.rhp` from the folder into an open instance of Rhino 6
- set all your Vray setting prior to running the script
- you should be able to type `badger` into the command prompt in Rhino to get the UI window open

# Place blocks on terrain

This script takes blocks of buildings that have been created on a flat plane and projects them onto a terrain
![](links/rhino-project01.png)

Script:

```
import rhinoscriptsyntax as rs

objs = rs.GetObjects(message="Select blocks to project to", filter=4096)
surf = rs.GetObject(message="Select surface terrain", filter=8+16+32)

rs.EnableRedraw(enable=False)

for i in objs:
    c = rs.BlockInstanceInsertPoint(i)
    crv = rs.AddCurve([c, [c[0],c[1],-1000]], degree=1)
    pt = rs.CurveBrepIntersect(crv, surf)
    if pt is not None:
        ptCoord = rs.PointCoordinates(pt[1])
        rs.CopyObject(i,[0,0,(ptCoord[2]+.35)]) # offset for block ht
        rs.DeleteObjects(pt[1])
    rs.DeleteObjects(crv)

rs.EnableRedraw(enable=True)
```
