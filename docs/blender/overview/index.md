# Description

Blender is a free  and open-source full fledged 3D suite capable of modeling, rigging, animation, simulation, rendering, compositing, motion tracking, video editing, game creation. It's feature set is similar to 3Ds Max, Maya, and Cinema 4D and others, with the added advantage that it's completely free. The features we'll be focusing on most are **modeling** and **rendering**. The guidelines below cover only what's necessary for our workflow. The full blender manual can be found [here](https://docs.blender.org/manual/en/dev/) and it may be necessary to reference it occasionally.

# General tips
The following a general tips that you should be aware in case you are not sure where to find something in Blender or what a specific element does

**Spacebar**

You can search for any available command by pressing the spacebar on your keyboard

![spacebar](links/space.JPG)

**Onine Manual**

If you right-click an any menu item, there is a link to the online manual, where it's linking to the section that contains the item you require assistance with.

![spacebar](links/right-click-manual.jpeg)


# User Interface

At first, Blender's UI may seem daunting, but it is cohesively composed of a 3d Window, properties, and outliner. Below are the highlights of each window type

**3D Viewport**

The 3d View is where we draw our 3d information, and where we can create or manipulate 3d objects.

![3D Viewport](links/blender-ui-01.jpeg)

Within the 3d view, there is the actual 3d window, and togeable sidebars on either side.

**3d View toolbar ("T")**

The toolbar is on the left-hand side, and it's useful for commands like adding or transforming geometry. The toolbar also give access to edit the parameters of the last command.

![Viewport Toolbar](links/blender-toolbar.png)

**3d View Properties ("N")**

The 3d view properties are located on the righ-hand side of the 3d viewport. They give access to editing information of the currently selected object, like location, rotation, and scale.

![Viewport Properties](links/blender-3dview-properties.png)

**Toggle 3d view Sidebars**
In case you don't see the sidebars, or you would like to hide them, the animation below shows how to toggle them by pressing the small "+" on either the left or right hand side and how to close them by dragging the outer edge inwards.

![Toggle sidebars](links\blender-tools-properties.gif)

## Properties

Within the properties, settings are exposed for rendering, world, materials, objects, and modifiers. In the default Blender configuration, the properties are located on the right hand side.

![Location of properties](links/blender-ui-02.jpeg)

There are multiple tabs within the properties window. The ones we will focus on are highlighted below.

![Properties tabs](links/blender-properties.jpeg)

Within each of the tabs we will initially focus on a few important functions

- Render properties - set render window size & render quality settings
- World settings - set up background either based on an HDRI image or a window
- Modifiers - add array, bevel, solidify, curve modifiers,and more
- Object Data - apply UVW for correct materials application
- Materials - Assign and modify materials to different faces

## Outliner

The outliner shows all the objects in either the current layer or the whole scene. The outliner is useful for controlling visibility, searching for specific objects, and renaming objects

![Outliner](links/blender-ui-03.jpeg)

Below is an example of how to soelect elements via the outliner

![Usage of the outliner](links/outliner.gif)

## Flexible UI
Windows, layers, file organisation

The use interface in Blender is highly flexible, with each type of UI window, capable of becoming any other type of window. Usually there is no need to modify the UI, however when working with materials it is important to understand how to view the Node editor.

Each window type can become any other window type - e.g. a 3d viewport can become properties and vice versa. In the header of each window, there's is an icon that shows the type of window that's currently open. By clicking on it, it's possible to select different menu types from the menu.

![window types](links/blender-ui-04-window-types.jpeg)

Let's take the example below. The Window types have been modified by splitting the 3d viewport in two and then assigning the bottom viewport into a node editor.

![window types](links/blender-ui-05-split.jpeg)

Here is a video demonstrating how the UI change can be achieved.

![window types](links/blender-ui-resize.gif)

## Default User Preferences

By default, Blender's settings are a little awkward. To make them more standardized and similar to other programs we use, modify them so they look like the following screens.

To toggle the user preferences, either go to **File -> User Preferences** or press "Ctrl"+"Alt"+"U"

Modify the highlighted View Manipulation Settings within the interface tab

![window types](links/blender-settings01b.jpeg)

Modify the Select with to be with the left mouse button in the input tab and make sure to click Save user Settings

![window types](links/blender-settings02b.jpeg)

# Basic Actions

## Import OBJ

To import an OBJ into blender, go to **File -> Import -> OBJ**. Make sure you modify the axes, so that the up axis is Z up, as displayed below.

![Import OBJ in Blender](links/export-obj.jpeg)

## Materials
- Overview of material definitions
- Avoiding tiling by overlaying multiple Materials
- UV Unwrap with Sure UVW addon will this

for more information, refer to [Material in Blender's Manual](https://docs.blender.org/manual/en/dev/render/blender_render/materials/assigning_a_material.html)

## World Settings

An HDRI image is essential to create a well lit scene. The best place to get HDRIs is [HDRI Haven}(https://hdrihaven.com/)

Process of creating an HDRI background scene:
- go to the world settings
- open the node editor
- drag and drop your HDRI image


# References
There are many good resources for finding information on how to do a specific task in blender. Below is a list of some of the most common ones.

- [UH Studio Youtube](https://www.youtube.com/uhstudio) - a channel about concpetual architectural design with Blender
- [Blender Stack Exchange](https://blender.stackexchange.com/) - question and answers relating on how to do things in Blender. You can browse and also ask new questions.
- [Blender Nation](http://www.blendernation.com) - The daily news blog for all things Blender. It has become a nice nersouce on its own as you can search within the site for useful information, as many people that do tutorials post them on the website
- Youtube - there are many channels devoted exclusively to blender
- [Blender Artists](https://blenderartists.org/) - The forum for Blender users. Has technical information, galleries, and many artists are quite warm to describe the process they used to create their illustrations.
