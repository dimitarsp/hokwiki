Blender to Rhino

With the support of n-gons in rhino 6, mesh objects start to appear much cleaner than they did in rhino 5, reducing the visual pollution, and allowing us to focus more on design.

comparison of same element in rhino 5, rhino 6, and blender

show n-gons support in rhino 6

then mesh2nurbs

then mergeAllFaces
