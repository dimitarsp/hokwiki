

The following settings are showing a rendering process for 360 degree images, capable of being viewed via any device inlcuding VR


# Settings required to be Modified

Adjust the following settings in the properties panel

## Render layers tab:
Enable stereoscropic rendering in the render layers tab

![Enable stereoscopic rendering](links/img01.jpeg)

## In the render tab:

Adjust Rendering size to a 2:1 ratio. Use 25% of final size for testing purposes
Change output views format to stereo 3d
Change stereo mode to top-bottom

![](links/img04.jpeg)

## Camera Settings

Once you have added and positioned a camera, adjust the following settings:

Change camera type to Panoramic
Select equirectangular from the drop down menu below the lens type
Check the spherical stereo option

![](links/img03.jpeg)

Once the image is saved you can either download an VR viewer app to view on a mobile device or use poly.google.com to create a 360 tour. Check soon for added information for different ways to view 360 degree renderings

Here is what the finished result looks like:

![](links/pano.jpg)

#### to add: embedded 360 renderings
