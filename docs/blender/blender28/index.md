Blender 2.8 is the new version currently in Beta from Blender. It is still the same free and open source general 3d animation suite, with some new exciting features.

The top new features are a revamped and more user-friendly interface, which will make it easier to start using the program, and a real-time rendering engine, similar to Unity/Unreal, with the added advantage that you can model and render in the same application.

This page will constantly be updated.
