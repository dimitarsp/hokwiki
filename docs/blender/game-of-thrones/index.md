![image](links/overview.jpg)

The goal of the visualiastion style is to create a unique portraial, a halfpoint between quick hidden-line renderings and outsourced finished renderings. The idea is to give a stylistic reference that appeals to the senses ("I can touch it") to something akin to a physical scaled model made out of wood.


There are a series of steps required to be able to create quick and efficient images.

Initially, a little prep work is needed within the Rhino file in order to get the best of the file conversion in Blender.

You might be asking why go through in Blender, when in Vray it's possible to create quite complex materials already. Blender has a node-based material editor that works similarly to Grasshopper, and allows for high amount of flexibility. Additionally, with the new realtime Eevee render engine in the upcoming Blender 2.8, subtle material variations can be previewed instantly without waiting to render a part of the model. Lastly, I have significant amount of experience with Blender which I am quite happy to share with you.

## File prep in Rhino
With some best practices, the conversion process from Rhino to Blender can be as seamless as possible. It's important to export **only the needed geometry**. The less clutter in the conversion process the easier it will go.

### Materials

Materials should be **as simple as needed for the conversion proces**s. Material names will transfer over, which will make it easier to assing materials for different elements, eg different context buildings should have all the same material.

It's also important to **name the materials appropriately**. For example a material for context buildings should be called _context buildings_.

### Exporting OBJ
With the selected objects, go to _File -> Export Selected_, and make sure to **save as OBJ file**. You should see the following window:

![OBJ Export options](links/obj-export.JPG)

***There are three ways to convert geometry from Rhino to Blender:***

- **Convert each Rhino layer to a single object in Blender**. This is the preferred way to work between the two programs. However, if the layers are quite complex in Rhino, there is no need to replicate the same complexity in Blender, and this case, it's better to use another option. In this case, when exporting to OBJ, **select _Layers as OBJ Groups is selected_** as shown above.
- **Convert Rhino groups to objects in Blender**. This option aleviates the pains of dealing with too many layers. Temporarily group objects for the export process. In this case **select _Groups as OBJ groups_**
- **Convert all exported elements into one object in Blender**. This is useful for exporting simpler elements. For example, if everything has been previously exported, and another user has modified a few elements, instead of re-exporting the whole scene, it is easier to export only the modified elements. In this case, **select _Do not export layer/group names_**.
- There is also the option to export each individual object as such to Blender but this is **not recommended**, as configuring the right materials to the right objects becomes difficult.



![Meshing Options](links/obj-export2.JPG)

Initially, it is recommended to use these settings, adjusting them only if the initial exported result does not givea good version.

## Import to Blender

### Starter File

For your convenience, a starter file with the necessary initial render settings and materials has been created. **It is recommended to open it in Blender and from within Blender, save it as in your new directory** in order to preserve links to the textures.

**Location of the startup file:**
`X:\Parametric Design\Public\wiki\resources\blender\game-of-thrones` and select the `.blend` file


### Import OBJ

To import your obj file, go to _File -> Import -> OBJ_. Make sure your up axis is Z as shown here:

![Import settings](links/export-obj.jpeg)

Once you have imported your geometry

### Materials

In case you didn't start with the startup file mentioned above, you can still use it as a reference library and load the material into your working file:

- In blender, with your file open, go to ***File -> Append*** and navigate to `X:\Parametric Design\Public\wiki\resources\blender\game-of-thrones` and select the `.blend` file
- Continue click `Materials`
- From this location, choose the materials you would like to append, and press _Append_.

![Append Materials](links/append-materials.gif)

The current list of materials includes:

- context buildings
- context roads
- context
- arena

_to do: add screenshots of the materials in closeup and aerial_


### UV Unwrap

UV Unwrapping is handled by an addon called **Sure UVW**, which takes care of the UV unwrapping process and gives all unwrapped opbjects the same texture scale.

#### Install Sure UVW

- download the addon from [**here**](https://dev-files.blender.org/file/data/iphjd4boxxkpwu5chrof/PHID-FILE-7y7gmdelsdfd2237dz56/sure_uvwbox51.py)
- In bender, go to **file -> user preferences -> addons** and press the button near the bottom called `Install add-on from file`.
- Navigate to your downloads folder and select the file, and then press the `Install add-on from file` button near to top right-hand corner
- once you are back in the user preferences, make sure you press `save user settings` befor closing the window

The above process need to be completed only once once.

#### Using Sure UVW

- make sure your object scale is 1 as in the screenshot below. If it isn't press `Ctrl+a` to apply scale and select `scale`. Now your scale should be 1.
![object scale](links/object-scale.png)
- once your scale is set 1, go to the data settings tab within the properties. Scroll all the way down to the bottom to see the UVW settings.
- Click `Show active texture on object` and then `UVW Box Map`
![uvw settings](links/uvw-usage.png)
- That's it. Now your object should display well.
- **Note:** If you change your geometry, you will need to reapply the UVW Box map by going through the process above again. Thus, it is **recommended to apply UVW Box map once you have your geometry set** for the current scheme or option.

#### Script for Box UVW

Chances are you have been working so hard on the model, that you may not have enough time to click through every single object. Luckily, I have written a small script that applies UVW Box Map to all  selects all the mesh objects


###
