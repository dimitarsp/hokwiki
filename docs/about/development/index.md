# screenshot tool - shareX, 8 January 2019

For writing documentation, it is essential to have the fastest and most intuitive workflow. One of the most important elements of a fast workflow is taking and annotating screenshots. Thus far, I have been using the default Windows snipping tool, saving a file and then opening it in [Gravit Desginer](https://www.designer,io) to annotate, annotating, and then exporting the file. These are quite a few steps to take for a simple screenshot with a rectangle.

Yesterday, I was taking some screenshots on my mac at home and noticed the the latest AppleOS version has a new screeshot tool that allows to annotate right after taking an image. After taking a few screenshots, I was sold and the simplicity and quick use. So, this morning, I set on finding a similar tool that works on windows. I have used [greenshot](http://getgreenshot.org/) before, but it's a bit cumbersome, and still requires a lot of steps. So, after going through the complete list of apps for screenshots, I tried [Lightshot](https://app.prntscr.com/en/index.html) in Chrome and it works well, but requires an admin to install it on windows. The crhome version is limited to taking screenshots only in the browser.


![](links/img01-lightshot.png)

## shareX

Then, I found another app called [**ShareX**](https://getsharex.com/), which can be downloaded as a portable version. This app allows to either annotate before taking the screenshot or open a lightweight image editor right after. It can save directly to a screenshot folder, copy to clipboard, and/or save to a specific folder. In other words, it can be as seamless and quick as pressing a button, annotating, and then saving. It also records GIFs and movies. So, I am quite happy to have invested to find an app that will in the long-term save significant amount of time.

![](links\img02-sharex.png)


# Alpha 1 release, 7 January 2019

This wiki is the effort of 2+ years of research, conversations around the office, and time "hacked" in order to start to make it a reality. A lot of research was done on modern wiki platforms, formatting, and availability of offline access. After trialing many platforms, the initial wiki release has been compiled using [MKDocs](https://www.mkdocs.org/), and all documents have been written in markdown, ensuring future interpolability with other wiki platforms, static site generators (SSG), and/or flat-file content management systems.

Offline access has been the single most important feature, as in its current proof-of-concept version, the wiki is hosted and accessible via direct folved access. It works just as well as being set up in a proper server access, except for the search functionality.

The theme currently used is based on Google's Material UI, and is being actively developed. It has most of the functions that I have researched to be good - vertical menu, and a table of contents based on the header tags in each of the documents.

The current limitations of the platform is that it has no blog feed for automatic summary, amd no front matter support for summaries, categoies, and tags.

I am looking to extend the base MKdocs package by adding the following features:

 - light box, to allow full-screen view of documents
 - [magnify lens](https://github.com/thdoan/magnify) to allow interactive zoom into large images
 - possible javascript automatic gallery based on a folder

Besides mkdocs, I have tested and looked into using the following other platforms:

- [Hugo](https://gohugo.io/) - this is a full fledged SSG, with blog feed, and tags capabilities. However, I've struggled to create folder access, and may be overly complex for our wiki needs at the moment
- [Grav CMS](https://getgrav.org/) - I am quite familiar with Grav as I use it for my personal website. It's the modern alternative of Wordpress and sits nicely between SSGs and wordpress. Like SSGs, it uses markdown files and a flat-file and folder based organisation. Like Wordpress, it has a nice GUI, with login details, that may be useful for less tech-savy users. However it cannot work without a server environment
- [Vuepress](https://vuepress.vuejs.org/) - This is a wiki relevant SSG, similar to MKdocs, with some basic blog capabilities. Dragos is continuing to actively look into this package. At the moment, we are pursuing whether it's possible to use it in a file-based environment
- [Docunasorus](https://docusaurus.io/) - This is another wiki SSG, quite similar Vuepress, with some documentation capabilities. I am actively testing its capabilities at the moment.

*Written by Dimitar*
