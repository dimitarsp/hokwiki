In here is a collection of simple libary samples that will be useful for many Projects

## Adding People

This sample allows you to easily add people as a particle scene in your view

![](links/people01.gif)

**Usage**

- Download and open [**this Blender file**](links/people01.blend)
- Copy the plane with the people with `Ctrl+C`
- Paste the plane with people in your working file with `Ctrl+V`
- In addition to the people on the plane, you may see two other people show up oriented along the X axis. It is essential that these objects do not get erased as they are the blocks that get repeated on the plane.
    - It is recommended to place them on a seperate hidden layer.
    - ![](links/people01.png)


**Painting influence**


<div class="gallery">
  <a href="links/people02.png" class="big"><img src="links/people02.png" /></a>
</div>



1. To adjust the area of influence, make sure your object is subdivided enough
- Create a vertex group and assign some vertices to the vertex group
- The the people particle settings to the object
- In the particles settings, scroll all the way down to vertex groups and assign your group for **density**
- Now switch from object or edit mode into weight paint mode
-
