
## Rhino Workshops
Relevant and pertinent information in regards to the workshops shall be put here

## Rhino Session 1

Held on 7 November 2018

### Video Recording

<video width="100%" height="450" controls>
  <source src="../../../resources/rhino/rhino-session01.mp4" type="video/mp4">
  Rhino Session 1
</video>

### Outline

Here is what we covered:

First session of hands-on training to get familiar with Rhino. You would ideally become familiar enough with the program to be able to create simple geometry and to know your way around when opening someone else's file. We will try to cover the agenda below with as many questions as you may have about the interface.

- overview of the interface
  - menus
  - toolbars
- creating & editing 2d elements
- layers
- display styles
- snaps
- layouts for printing

As we mentioned this will be part of a series, with a subsequent Rhino workshop covering more advanced topics, after which we will have intro to grasshopper sessions.

## Rhino Session 2

Held on 13 November 2018

### Video Recording

<video width="100%" height="450" controls>
  <source src="../../../resources/rhino/rhino-session02.mp4" type="video/mp4">
  Rhino Session 2
</video>

### Contents covered

We will continue with Rhino basics and cover most aspects you would need for working with Rhino. After this session, you should feel comfortable to execute studies within rhino and plugging them back into your team's working environment. Also, after this session, we will be ready to explore grasshopper.


I suggest you play around with something relevant to what you are working on at the moment or just play and see if there is something you would like to do but you are not sure how to do it.

Topics to cover:

- blocks
- importing/exporting
- attachments (like xrefs in autocad)
- text/labels/dimensions
- cplanes (like UCS in autocad)
- printing well from rhino
- capture view without printing dialogue
- saving views
- advanced layer operations
- layer states
- clipping planes
- arctic mode in depth


## Rhino Session 3

Held on 20 November 2018

### Video

<video width="100%" height="450" controls>
  <source src="../../../resources/rhino/rhino-session03.mp4" type="video/mp4">
  Rhino Session 3
</video>

### Contents covered

Now that we have the basics covered, in this third session, we will look at a practical hands-on example of building up a model and a concept in our 1.5 hour session. My intention is that you work mostly independently and I walk around to help you with any questions you may have. We will look at a few potential masterplan sites and do the following:

- Set up our working environment
- Trace context from google earth or import from osmaps
  - create project boundary with points
  - work with grasshopper to fillet boundary line and to offset

This is as far as we got in this session, but we did have an initial introduction to grasshopper. Let's build on the foundation that we set and **try to achieve the following for the next session**:

- Create a simple masterplan proposal and get areas and floors with or withouth some very basic assistance from grasshopper
- Focus on a feature building or a pavilion within your masterplan
-- Get creative designing and modelling
-- This would be ideal to learn the modelling process in Rhino in a bit more detail
- Documenting our process in both scaled plans and views

Here is the satellite image from today's session:

![](links/satellite.png)

Note that at this point, it is up to you and the effort that you put into further developing your rhino skills. Beyond what we have covered, here are two tutorial series that are highly useful from one of my tutors and are complementary to what we have already learned.

- [Rhino tutorial series on modeling a zaha hadid building](https://www.plethora-project.com/education/2017/5/31/rhino-nurbs-modeling)
- [Grasshopper tutorial series learning about data, lists, etc](https://www.plethora-project.com/education/2017/5/31/rhino-grasshopper)
