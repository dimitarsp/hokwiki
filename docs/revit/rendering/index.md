The two main recommended rendering processes from Revit consist of using VRay and Enscape. Both processes rely on creating good materials within Revit, with appropriate textures, where possible. V-Ray can be used to create very high quality visuals at the expense of longer set-up and rendering timeframes. Enscape renders in real time and can be used to produce both stills and walkthroughs.

## Examples of Vray projects [to do]

### Kuwait University


## Example of Enscape projects [to do]

### Kuwait University

### Brabazon Bristol Hangar Arena Competition
