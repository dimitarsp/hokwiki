Examples of Grasshopper script snippets to help you get started with Grasshopper


## Enscape people

<!-- <img src="links/peopleEnscape01.png" class="zoom" data-magnify-src="links/peopleEnscape01.png"> -->

![](links/peopleEnscape01.png)

**What it does:** This is a script snippet that allows you to populate a surface with people. The people are blocks, which need to be loaded into your file. The easiest way is to open the rhino file from below, copy the blocks to your file and then run the grasshopper script.

Links:

- [Rhino file](links/peopleEnscape01.3dm)
- [Grasshopper file](links/peopleEnscape01.gh)


## Blocks in Grasshopper

<img src="links/blocks01.png" width=50% style="float:right; padding-top:0; margin-right:10px;"> **What it does:** Grasshopper doesn't have native blocks capabilities. There are a variety of ways to work with block through grasshopper plugins, however, the easiest way that I have found to work with blocks is through this custom C# node.

[Grasshopper script](links/blocks01.gh)

## Urban Design Script

![Urban Design Area Tool](links/urbandesignarea01.png)

**What it does:** Takes the footprints of your file and allows you to adjust heights/floors mixture to get the required GFA

Links:

- [Rhino file](links/urbandesignarea01.3dm)
- [Grasshopper file](links/urbandesignarea01.gh)


## Attractors

This is a script to work with attractor-based grid deformation.

![Urban Design Area Tool](links/planningattractors01.png)

<video width="100%" height="450" controls>
  <source src="../../../resources/grasshopper/planningattractors01.mp4" type="video/mp4">
  Grasshopper Example Script
</video>

Links:

- [Rhino file](links/planningattractors01.3dm)
- [Grasshopper file](links/planningattractors01.gh)

## Masterplanning Script

![](links/masterplanning01.png)

**What it does:** starting with the centerlines for different roads, it allows you to create urban plots

Links:

- [Rhino file](links/masterplanning01.3dm)
- [Grasshopper file](links/masterplanning01.gh)

## Split List randomly into 3 lists

This scripts is useful for getting random people in the same area, or random buildings from a list of multiple placement points

![Grasshopper file](links\splitRandomThreeLists.png)

Links:

- [Grasshopper file](links\splitRandomThreeLists.gh)


## Automated baking per layer

[to do]

## Store and recall GH options from excel

[to do]
