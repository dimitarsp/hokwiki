In this space, you can find general tips and information about Revit

# New to revit

If you are new to Revit, in addition to having in-house workshops with Paul, visit [linkedin.com/learning](https://www.linkedin.com/learning) for basic training courses.
