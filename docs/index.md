# Hello

Welcome to our own **HOK London** Wiki!

We will publish tips, tricks, user guides, and 'hacks' to  improve our workflow processes so we can continue to improve the quality of our work. In addition, we have listed champions for various workflows

## Revit

### Project set up & most questions

Champions: [Paul Duggleby] Adam

Rendering

- Rendering with Vray - Dragos
- Rendering with Enscape:
    - Paul Duggleby
    - Dragos

## Rhino

Champions:

- Carlos de la B
- Dimitar

## Grasshopper

Champions

- Carlos de la B
- Dimitar

