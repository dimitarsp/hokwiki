There are many possible ways to go from Rhino to Revit. Depending on what you may need to achieve in Revit from your Rhino model, your workflow may vary significantly.

## Direct Methods

These methods are useful for getting masses or generic initial models for detailing

### Import Rhino files in Revit

From version 2018, Revit can open Rhino files natively. However, support for complex surface is spotty, at best. Nevertheless, for simple elements like walls, and simple surfaces that are fairly untrimmed, Revit should be able to import a Rhino file directly.

### Import ACIS files in Revit

If Revit fails to import a Rhino file, ACIS (.sat) files are the next best option to try. In my experience, .sat files tend to work a little better than Rhino files when importing geometry into Revit. In this case, the process is two-fold:

- from Rhino, select to objects you wish to export, and go to `file -> export selected`. Choose `ACIS (SAT)` files
- in Revit, go to `import CAD`, and make sure you change from dwg to sat file: ![](links/revitsat.png)

## Grasshopper Methods

These methods are a little more involved, but allow for native Revit elements - walls, floors, adaptive components, families, grids, etc.
Both plugins install packages for Grasshopper and for Revit.

### Using Hummingbird

[Hummingbird](https://www.food4rhino.com/app/hummingbird) exports a text file that is then imported into Revit. It's robust, albeit a little inovled in the initial setting up. We have used it to populate sports arenas with native Revit families.

### Using Grevit

[Grevit](https://www.food4rhino.com/app/grevit-grasshopper-native-bim) is generally faster and simpler to use compared to Hummingbird, especially for thousands of elements. However, it does not provide a way to create rotation of families (instead, we have previously created a custom family paratemeter to deal with families rotation).
